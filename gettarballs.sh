#!/bin/bash
# TODO check if index file exists

FILEINDEX="f.txt"
BASEURL="https://download.fcitx-im.org/fcitx5"
cat f.txt | while read -r line; do \
    if [ ! -d $line ]; then
        mkdir $line
    fi
    cd $line
    FILENAME=$(curl "$BASEURL/$line/" | pandoc -f html -t plain | grep -o "$line.*\.tar\.xz" | sort -V | tail -n 1)
    wget "$BASEURL/$line/$FILENAME"
    cd ..
done
